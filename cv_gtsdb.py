import torch
import cv2
import numpy as np
import pathlib as pl
import json
from dataclasses import dataclass

from typing import List, Dict, Tuple

@dataclass
class Box:
    class_index: int
    class_name: str
    corners: np.ndarray

    def __repr__(self):
        return "[class=%s (%f,%f,%f,%f)]" % (self.class_name, self.corners[0], self.corners[1], self.corners[2], self.corners[3])

    def __str__(self):
        return repr(self)

class Dataset(torch.utils.data.Dataset):
    def __init__(self, images_path, labels_path, width, height, classes, transforms=None):
        self.transforms = transforms
        self.images_path = images_path
        self.annotations_dir = labels_path
        self.height = height
        self.width = width
        self.classes = classes

        self.samples_view = self.read_samples()

    def read_samples(self):
        gt_boxes = []
        for split_type in ['gtsdb_valid', 'gtsdb_train']:
            with open(self.annotations_dir / f'{split_type}.json') as gt_boxes_file:
                gt_boxes_json = json.load(gt_boxes_file)

            for annotation_id, gt_boxes_list in gt_boxes_json.items():
                gt_boxes.append((annotation_id,
                                list(map(lambda b: Box(class_index=b['class_index'],
                                                    class_name=b['class_name'],
                                                    corners=np.array(b['corners']).astype(np.float32)), gt_boxes_list))))
        return gt_boxes

    def __getitem__(self, idx) -> Tuple[torch.Tensor, Dict]:
        image_id, gt_boxes = self.samples_view[idx]
        image_path = self.images_dir / f'{image_id}.ppm'

        image = cv2.imread(image_path.as_posix())

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).astype(np.float32)
        image_resized = cv2.resize(image, (self.width, self.height))
        image_resized /= 255.0

        boxes = []
        labels = []

        image_width = image.shape[1]
        image_height = image.shape[0]

        for gt_box in gt_boxes:
            labels.append(gt_box.class_index)

            ymin = gt_box.corners[0]
            xmin = gt_box.corners[1]
            ymax = gt_box.corners[2]
            xmax = gt_box.corners[3]

            xmin_final = (xmin/image_width)*self.width
            xmax_final = (xmax/image_width)*self.width
            ymin_final = (ymin/image_height)*self.height
            ymax_final = (ymax/image_height)*self.height

            boxes.append([xmin_final, ymin_final, xmax_final, ymax_final])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        iscrowd = torch.zeros((boxes.shape[0],), dtype=torch.int64)
        labels = torch.as_tensor(labels, dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["area"] = area
        target["iscrowd"] = iscrowd
        image_id = torch.tensor([idx])
        target["image_id"] = image_id
        # apply the image transforms
        if self.transforms:
            sample = self.transforms(image=image_resized,
                                     bboxes=target['boxes'],
                                     labels=labels)
            image_resized = sample['image']
            target['boxes'] = torch.Tensor(sample['bboxes'])

        return image_resized, target

    def __len__(self):
        return len(self.samples)