import json
from operator import index
import random
from tensorflow import keras
import numpy as np
from pathlib import Path
import pathlib as pl

from .training_sample import Box
from .training_sample import TrainingSample
from . import image
from ..models import anchors


class Dataset:
    def __init__(self, split: str, images_path: str = 'D:\\mapillary\\images', annotations_path: str = 'D:\\mapillary\\mtsd_v2_fully_annotated', shuffle=False):
        """
        Parameters
        ----------
        split : str
          Dataset split to use: train, val or test
        images_path : str
          Path to images location
        annotations_path : str
          Path to annotations folder
        """
        self.split = split
        self.images_dir = pl.Path(images_path)
        self.annotations_dir = pl.Path(annotations_path)
        print('paths inited')

        self.index_to_class = self._get_classes()
        self.class_to_index = {class_name: class_index for (
            class_index, class_name) in self.index_to_class.items()}
        print(f'classes ready: {len(self.index_to_class)}')

        #self.samples = self._get_samples()
        #print('samples ready')

        self.i = 0
        self.feature_pixels = 16
        self.shuffle = shuffle
        self.augment = True
        print('base iter config ready')

        self._gt_boxes_by_anno_id = self._get_ground_truth_boxes()
        self.num_samples = len(self._gt_boxes_by_anno_id)
        self.samples = [*self._gt_boxes_by_anno_id.keys()]
        print('ground truths ready')

    def __iter__(self):
        self.i = 0
        if self.shuffle:
            random.shuffle(self.samples)
        return self

    def __next__(self):
        if self.i >= len(self.samples):
            raise StopIteration

        annotation = self.samples[self.i]
        self.i += 1
        flip = random.randint(0, 1) != 0 if self.augment else 0
        sample = self._generate_training_sample(annotation, flip)

        return sample

    def _get_samples(self):
        with open(self.annotations_dir / 'splits' / f'{self.split}.txt', 'r') as f_split:
            samples = [l.rstrip() for l in f_split]
        return samples

    def _generate_training_sample(self, annotation, flip):
        # Load and preprocess the image
        filepath = self.images_dir / f'{annotation}.jpg'
        scaled_image_data, scaled_image, scale_factor, original_shape = image.load_image(
            url=filepath, min_dimension_pixels=600, horizontal_flip=flip)
        _, original_height, original_width = original_shape

        # Scale ground truth boxes to new image size
        scaled_gt_boxes = []
        for box in self._gt_boxes_by_anno_id[annotation]:
            if flip:
                corners = np.array([
                    box.corners[0],
                    original_width - 1 - box.corners[3],
                    box.corners[2],
                    original_width - 1 - box.corners[1]
                ])
            else:
                corners = box.corners
            scaled_box = Box(
                class_index=box.class_index,
                class_name=box.class_name,
                corners=corners * scale_factor
            )
            scaled_gt_boxes.append(scaled_box)

        # Generate anchor maps and RPN truth map
        anchor_map, anchor_valid_map = anchors.generate_anchor_maps(
            image_shape=scaled_image_data.shape, feature_pixels=self.feature_pixels)
        gt_rpn_map, gt_rpn_object_indices, gt_rpn_background_indices = anchors.generate_rpn_map(
            anchor_map=anchor_map, anchor_valid_map=anchor_valid_map, gt_boxes=scaled_gt_boxes)

        # Return sample
        return TrainingSample(
            anchor_map=anchor_map,
            anchor_valid_map=anchor_valid_map,
            gt_rpn_map=gt_rpn_map,
            gt_rpn_object_indices=gt_rpn_object_indices,
            gt_rpn_background_indices=gt_rpn_background_indices,
            gt_boxes=scaled_gt_boxes,
            image_data=scaled_image_data,
            image=scaled_image,
            filepath=filepath
        )

    def _get_classes(self):
        classes_file = self.annotations_dir / 'sign_names.txt'
        with open(classes_file, 'r') as fc:
            index_to_class = {i: c.rstrip() for i, c in enumerate(fc)}
        return index_to_class

    def _get_ground_truth_boxes(self):
        gt_boxes = dict()
        with open(self.annotations_dir / f'{self.split}.json') as gt_boxes_file:
            gt_boxes_json = json.load(gt_boxes_file)

        for annotation_id, gt_boxes_list in gt_boxes_json.items():
            gt_boxes[annotation_id] = list(map(lambda b: Box(
                class_index=b['class_index'], class_name=b['class_name'], corners=np.array(b['corners']).astype(np.float32)), gt_boxes_list))
        return gt_boxes

    # def _get_ground_truth_boxes(self):
    #   # parse all ground truth boxes to a dict.
    #   gt_boxes = dict()
    #   empty_ids = []
    #   print(f'generating ground truths for {len(self.samples)} samples')
    #   for annotation_id in self.samples:
    #     anno_path = self.annotations_dir / 'annotations' / f'{annotation_id}.json'
    #     if not anno_path.exists():
    #       empty_ids.append(annotation_id)
    #       continue

    #     with open(anno_path, 'r') as anno_file:
    #       annotation = json.load(anno_file)

    #     boxes = []
    #     for sign in annotation['objects']:
    #       label = sign['label']
    #       if label == 'other-sign':
    #         continue
    #       class_name = label.split('--')[1]
    #       bbox = sign['bbox']
    #       corners = np.array([ bbox['ymin'], bbox['xmin'], bbox['ymax'], bbox['xmax'] ]).astype(np.float32)
    #       box = Box(class_index=self.class_to_index[class_name], class_name=class_name, corners=corners)
    #       boxes.append(box)

    #     if len(boxes) == 0:
    #       empty_ids.append(annotation_id)
    #       continue

    #     gt_boxes[annotation_id] = boxes

    #   print(f'{len(empty_ids)} empty ids found in the dataset. Removing...')
    #   for item in empty_ids:
    #     self.samples.remove(item)
    #   return gt_boxes

    num_classes = 235
    index_to_class = {0: 'accident-area',
                      1: 'accidental-area-unsure',
                      2: 'added-lane-right',
                      3: 'airport',
                      4: 'bicycles-crossing',
                      5: 'bicycles-only',
                      6: 'bike-route',
                      7: 'both-directions',
                      8: 'bus-stop',
                      9: 'bus-stop-ahead',
                      10: 'buses',
                      11: 'buses-only',
                      12: 'camp',
                      13: 'central-lane',
                      14: 'chevron-left',
                      15: 'chevron-right',
                      16: 'chevron-right-unsure',
                      17: 'children',
                      18: 'crossroads',
                      19: 'crossroads-with-priority-to-the-right',
                      20: 'curve-left',
                      21: 'curve-right',
                      22: 'dead-end',
                      23: 'dead-end-except-bicycles',
                      24: 'detour-left',
                      25: 'dip',
                      26: 'disabled-persons',
                      27: 'distance',
                      28: 'divided-highway-ends',
                      29: 'do-not-block-intersection',
                      30: 'do-not-stop-on-tracks',
                      31: 'domestic-animals',
                      32: 'double-curve-first-left',
                      33: 'double-curve-first-right',
                      34: 'double-reverse-curve-right',
                      35: 'double-turn-first-right',
                      36: 'dual-lanes-go-straight-on-left',
                      37: 'dual-lanes-go-straight-on-right',
                      38: 'dual-lanes-right-turn-or-go-straight',
                      39: 'dual-lanes-turn-left-no-u-turn',
                      40: 'dual-lanes-turn-left-or-straight',
                      41: 'dual-lanes-turn-right-or-straight',
                      42: 'dual-path-bicycles-and-pedestrians',
                      43: 'dual-path-pedestrians-and-bicycles',
                      44: 'emergency-facility',
                      45: 'emergency-vehicles',
                      46: 'end-of-bicycles-only',
                      47: 'end-of-built-up-area',
                      48: 'end-of-buses-only',
                      49: 'end-of-limited-access-road',
                      50: 'end-of-living-street',
                      51: 'end-of-maximum-speed-limit-30',
                      52: 'end-of-maximum-speed-limit-70',
                      53: 'end-of-motorway',
                      54: 'end-of-no-parking',
                      55: 'end-of-pedestrians-only',
                      56: 'end-of-priority-road',
                      57: 'end-of-prohibition',
                      58: 'end-of-speed-limit-zone',
                      59: 'equestrians-crossing',
                      60: 'except-bicycles',
                      61: 'extent-of-prohibition-area-both-direction',
                      62: 'falling-rocks-or-debris-right',
                      63: 'flaggers-in-road',
                      64: 'food',
                      65: 'gas-station',
                      66: 'give-way-to-oncoming-traffic',
                      67: 'go-left',
                      68: 'go-right',
                      69: 'go-straight',
                      70: 'go-straight-or-turn-left',
                      71: 'go-straight-or-turn-right',
                      72: 'hairpin-curve-left',
                      73: 'hairpin-curve-right',
                      74: 'height-limit',
                      75: 'height-restriction',
                      76: 'highway-exit',
                      77: 'highway-interstate-route',
                      78: 'horizontal-alignment-left',
                      79: 'horizontal-alignment-right',
                      80: 'hospital',
                      81: 'interstate-route',
                      82: 'junction-with-a-side-road-acute-left',
                      83: 'junction-with-a-side-road-acute-right',
                      84: 'junction-with-a-side-road-perpendicular-left',
                      85: 'junction-with-a-side-road-perpendicular-right',
                      86: 'kangaloo-crossing',
                      87: 'keep-left',
                      88: 'keep-right',
                      89: 'lane-control',
                      90: 'left-turn-yield-on-green',
                      91: 'limited-access-road',
                      92: 'living-street',
                      93: 'lodging',
                      94: 'loop-270-degree',
                      95: 'maximum-speed-limit-10',
                      96: 'maximum-speed-limit-100',
                      97: 'maximum-speed-limit-110',
                      98: 'maximum-speed-limit-120',
                      99: 'maximum-speed-limit-15',
                      100: 'maximum-speed-limit-20',
                      101: 'maximum-speed-limit-25',
                      102: 'maximum-speed-limit-30',
                      103: 'maximum-speed-limit-35',
                      104: 'maximum-speed-limit-40',
                      105: 'maximum-speed-limit-45',
                      106: 'maximum-speed-limit-5',
                      107: 'maximum-speed-limit-50',
                      108: 'maximum-speed-limit-55',
                      109: 'maximum-speed-limit-60',
                      110: 'maximum-speed-limit-65',
                      111: 'maximum-speed-limit-70',
                      112: 'maximum-speed-limit-75',
                      113: 'maximum-speed-limit-80',
                      114: 'maximum-speed-limit-90',
                      115: 'maximum-speed-limit-led-100',
                      116: 'maximum-speed-limit-led-60',
                      117: 'maximum-speed-limit-led-80',
                      118: 'minimum-safe-distance',
                      119: 'minimum-speed-40',
                      120: 'mopeds-and-bicycles-only',
                      121: 'motorway',
                      122: 'narrow-bridge',
                      123: 'no-bicycles',
                      124: 'no-buses',
                      125: 'no-entry',
                      126: 'no-hawkers',
                      127: 'no-heavy-goods-vehicles',
                      128: 'no-heavy-goods-vehicles-or-buses',
                      129: 'no-left-turn',
                      130: 'no-mopeds-or-bicycles',
                      131: 'no-motor-vehicle-trailers',
                      132: 'no-motor-vehicles',
                      133: 'no-motor-vehicles-except-motorcycles',
                      134: 'no-motorcycles',
                      135: 'no-overtaking',
                      136: 'no-overtaking-by-heavy-goods-vehicles',
                      137: 'no-parking',
                      138: 'no-parking-or-no-stopping',
                      139: 'no-pedestrians',
                      140: 'no-pedestrians-or-bicycles',
                      141: 'no-right-turn',
                      142: 'no-stopping',
                      143: 'no-straight-through',
                      144: 'no-turn-on-red',
                      145: 'no-turns',
                      146: 'no-u-turn',
                      147: 'no-vehicles-carrying-dangerous-goods',
                      148: 'obstacle-delineator',
                      149: 'offset-roads',
                      150: 'one-direction-left',
                      151: 'one-direction-right',
                      152: 'one-way-left',
                      153: 'one-way-right',
                      154: 'one-way-straight',
                      155: 'other-danger',
                      156: 'parking',
                      157: 'parking-restrictions',
                      158: 'pass-left-or-right',
                      159: 'pass-on-either-side',
                      160: 'pass-right',
                      161: 'passing-lane-ahead',
                      162: 'pedestrian-stumble-train',
                      163: 'pedestrians-crossing',
                      164: 'pedestrians-only',
                      165: 'playground',
                      166: 'priority-over-oncoming-vehicles',
                      167: 'priority-road',
                      168: 'priority-route-at-intersection',
                      169: 'radar-enforced',
                      170: 'railroad-crossing',
                      171: 'railroad-crossing-with-barriers',
                      172: 'railroad-crossing-without-barriers',
                      173: 'railroad-intersection',
                      174: 'restricted-zone',
                      175: 'reversible-lanes',
                      176: 'road-bump',
                      177: 'road-closed',
                      178: 'road-closed-to-vehicles',
                      179: 'road-narrows',
                      180: 'road-narrows-left',
                      181: 'road-narrows-right',
                      182: 'road-widens',
                      183: 'road-widens-right',
                      184: 'roadworks',
                      185: 'roundabout',
                      186: 'safety-area',
                      187: 'school-zone',
                      188: 'shared-lane-motorcycles-bicycles',
                      189: 'shared-path-bicycles-and-pedestrians',
                      190: 'shared-path-pedestrians-and-bicycles',
                      191: 'slippery-motorcycles',
                      192: 'slippery-road-surface',
                      193: 'stairs',
                      194: 'steep-ascent',
                      195: 'stop',
                      196: 'stop-ahead',
                      197: 'stop-here-on-red-or-flashing-light',
                      198: 'stop-signals',
                      199: 't-roads',
                      200: 'telephone',
                      201: 'text-four-lines',
                      202: 'texts',
                      203: 'tow-away-zone',
                      204: 'traffic-merges-left',
                      205: 'traffic-merges-right',
                      206: 'traffic-signals',
                      207: 'trail-crossing',
                      208: 'trailer-camping',
                      209: 'tram-bus-stop',
                      210: 'trams-crossing',
                      211: 'triple-lanes-turn-left-center-lane',
                      212: 'truck-speed-limit-60',
                      213: 'trucks',
                      214: 'trucks-crossing',
                      215: 'trucks-turn-right',
                      216: 'turn-left',
                      217: 'turn-left-ahead',
                      218: 'turn-right',
                      219: 'turn-right-ahead',
                      220: 'turning-vehicles-yield-to-pedestrians',
                      221: 'two-way-traffic',
                      222: 'u-turn',
                      223: 'uneven-road',
                      224: 'uneven-roads-ahead',
                      225: 'weight-limit',
                      226: 'weight-limit-with-trucks',
                      227: 'width-limit',
                      228: 'wild-animals',
                      229: 'winding-road-first-left',
                      230: 'winding-road-first-right',
                      231: 'wombat-crossing',
                      232: 'wrong-way',
                      233: 'y-roads',
                      234: 'yield'}
